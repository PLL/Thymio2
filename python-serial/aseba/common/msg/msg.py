#
#	Aseba - an event-based framework for distributed robot control
#	Copyright (C) 2013:
#		Zievi Ursin Soler <usoler at student dot ethz dot ch>
#	Copyright (C) 2007--2012:
#		Stephane Magnenat <stephane at magnenat dot net>
#		(http://stephane.magnenat.net)
#		and other contributors, see authors.txt for details
#	
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Lesser General Public License as published
#	by the Free Software Foundation, version 3 of the License.
#	
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Lesser General Public License for more details.
#	
#	You should have received a copy of the GNU Lesser General Public License
#	along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import aseba.common.consts as consts
import ctypes
import struct

#\defgroup msg Messages exchanged over the network
#	
#	Aseba messages payload data must be 512 bytes or smaller, which means that the total
#	packets size (len + source + type + payload) must be 518 bytes or smaller.


#//! Parent class of any message exchanged over the network
class Message():
# TODO: is there a better datatype may be bytearray() ?
	rawData = ""  #std::vector<uint8>

	def __init__(self, t=None):
		self.source = consts.ASEBA_DEST_DEBUG	#uint16
		self.type = t							#uint16
		self.readPos = 0						#size_t

	def serialize(self, stream):
		self.rawData = ""
		self._serializeSpecific()
		l = len(self.rawData)				#uint16

		if (l > consts.ASEBA_MAX_EVENT_ARG_SIZE):
			#cerr << "Message::serialize() : fatal error: message size exceed maximum packet size.\n";
			#cerr << "message payload size: " << len << ", maximum packet payload size (excluding type): " << ASEBA_MAX_EVENT_ARG_SIZE << ", message type: " << hex << showbase << type << dec << noshowbase;
			#cerr << endl;
			assert()
		l = struct.pack('<H', l)			# header: length of message (word)
		stream.write(l)						#
		t = struct.pack('<H', self.source)	# header: source id of message (word/uint16)
		stream.write(t)						#
		t = struct.pack('<H', self.type)	# header: type of message (word/uint16)
		stream.write(t)						#
		if self.rawData:					# payload: the message
			stream.write(self.rawData)		#

		return

	def receive(self, stream):
		#// read header
		l = stream.read(2)							#uint16
		l = struct.unpack('<H', l)[0]				#
		source = stream.read(2)						#uint16
		source = struct.unpack('<H', source)[0]		#
		t = stream.read(2)							#uint16
		t = struct.unpack('<H', t)[0]				#

		#// create message
		message = MessageTypesInitializer().createMessage(t)

		#// preapare message
		message.source = source
		message.type = t
		if l:
			message.rawData = stream.read(l)
		message.readPos = 0

		#// deserialize it
		message._deserializeSpecific()

		if (message.readPos != len(message.rawData)):
			print "Message::receive() : fatal error: message not fully read."
			print "type: 0x%04X, readPos: %i, rawData size: %i" % (t, message.readPos, len(message.rawData))
			#message->dumpBuffer(wcerr);
			assert()

		return message
	
	#void dump(std::wostream &stream) const;
	#void dumpBuffer(std::wostream &stream) const;
	
	def _serializeSpecific(self):
		return

	def _deserializeSpecific(self):
		return

	def _add(self, T, val):
		if T == ctypes.c_uint16:
			#pos = len(self.rawData)
			self.rawData += struct.pack('<H', val)
		elif isinstance(T, str):
			if len(val) > 255:
				#cerr << "Message::add<string>() : fatal error, string length exceeds 255 characters.\n";
				#cerr << "string size: " << val.length();
				#cerr << endl;
				#dumpBuffer(wcerr);
				assert()
			
			self.rawData += struct.pack('<B', len(val))	# same as 'chr'
			self.rawData += val
		else:
			raise NotImplementedError("_add of type %s was not implemented yet!" % type(T))
	
	def _get(self, T):
		if isinstance(T, str):
			l   = self._get(ctypes.c_uint8)
			res = self.rawData[self.readPos:self.readPos+l]
			self.readPos += l
		else:
			cast = { ctypes.c_uint8:  '<B',
			         ctypes.c_int16:  '<h',
			         ctypes.c_uint16: '<H', }

			if (self.readPos + ctypes.sizeof(T)) > len(self.rawData):
				#cerr << "Message<" << typeid(T).name() << ">::get() : fatal error: attempt to overread.\n";
				#cerr << "type: " << type << ", readPos: " << readPos << ", rawData size: " << rawData.size() << ", element size: " << sizeof(T);
				#cerr << endl;
				#dumpBuffer(wcerr);
				assert()
		
			pos = self.readPos
			self.readPos += ctypes.sizeof(T)
			val = self.rawData[pos:pos+ctypes.sizeof(T)]
			res = struct.unpack(cast[T], val)[0]

		return res


#//! Any message sent by a script on a node
class UserMessage(Message):
	data = []	#std::vector<sint16>

	def __init__(self, t=None, data=None, length=None):
		Message.__init__(self, consts.ASEBA_MESSAGE_INVALID)
		if t:
			self.data = data
	
	def _serializeSpecific(self):
		return

	def _deserializeSpecific(self):
		return

	
#//! Request nodes to send their description
class GetDescription(Message):
	def __init__(self):
		Message.__init__(self, consts.ASEBA_MESSAGE_GET_DESCRIPTION)
		self.version = consts.ASEBA_PROTOCOL_VERSION		#uint16
	
	def _serializeSpecific(self):
		self._add(ctypes.c_uint16, self.version)

	def _deserializeSpecific(self):
		self.version = self._get(ctypes.c_uint16)


#//! Description of a node, local events and native functions are omitted and further received by other messages
class Description(Message):#, TargetDescription):
	def __init__(self):
		Message.__init__(self, consts.ASEBA_MESSAGE_DESCRIPTION)
	
	def _serializeSpecific(self):
		#self._add(str(), WStringToUTF8(self.name))
		self._add(str(), self.name)
		self._add(ctypes.c_uint16, self.protocolVersion)

		self._add(ctypes.c_uint16, self.bytecodeSize)
		self._add(ctypes.c_uint16, self.stackSize)
		self._add(ctypes.c_uint16, self.variablesSize)

		self._add(ctypes.c_uint16, self.namedVariables_size)
		#// named variables are sent separately

		self._add(ctypes.c_uint16, self.localEvents_size)
		#// local events are sent separately

		self._add(ctypes.c_uint16, self.nativeFunctions_size)
		#// native functions are sent separately

	def _deserializeSpecific(self):
		# 'UTF8ToWString' from common/utils/utils.h or .../utils.cpp
		#self.name = ''.join([struct.pack('<H', ord(a)) for a in self._get(string).decode('UTF8')])
		self.name = self._get(str())
		self.protocolVersion = self._get(ctypes.c_uint16)

		self.bytecodeSize = self._get(ctypes.c_uint16)
		self.stackSize = self._get(ctypes.c_uint16)
		self.variablesSize = self._get(ctypes.c_uint16)

		self.namedVariables_size = self._get(ctypes.c_uint16)
		#// named variables are received separately

		self.localEvents_size = self._get(ctypes.c_uint16)
		#// local events are received separately

		self.nativeFunctions_size = self._get(ctypes.c_uint16)
		#// native functions are received separately


#//! Description of a named variable available on a node, the description of the node itself must have been sent first
class NamedVariableDescription(Message):#, TargetDescription.NamedVariable):
	def __init__(self):
		Message.__init__(self, consts.ASEBA_MESSAGE_NAMED_VARIABLE_DESCRIPTION)
	
	def _serializeSpecific(self):
		self._add(ctypes.c_sint16, self.size)
		#self._add(str(), WStringToUTF8(self.name))
		self._add(str(), self.name)

	def _deserializeSpecific(self):
		self.size = self._get(ctypes.c_uint16)
		#self.name = UTF8ToWString(self._get(str()))
		self.name = self._get(str())


#//! Description of a local event available on a node, the description of the node itself must have been sent first
class LocalEventDescription(Message):#, TargetDescription.LocalEvent):
	def __init__(self):
		Message.__init__(self, consts.ASEBA_MESSAGE_LOCAL_EVENT_DESCRIPTION)
	
	def _serializeSpecific(self):
		#self._add(str(), WStringToUTF8(self.name))
		self._add(str(), self.name)
		#self._add(str(), WStringToUTF8(self.description))
		self._add(str(), self.description)

	def _deserializeSpecific(self):
		#self.name = UTF8ToWString(self._get(str()))
		self.name = self._get(str())
		#self.description = UTF8ToWString(self._get(str()))
		self.description = self._get(str())


#//! Description of a native function available on a node, the description of the node itself must have been sent first
class NativeFunctionDescription(Message):#, TargetDescription.NativeFunction):
	def __init__(self):
		Message.__init__(self, consts.ASEBA_MESSAGE_NATIVE_FUNCTION_DESCRIPTION)
	
	def _serializeSpecific(self):
		#self._add(str(), WStringToUTF8(self.name))
		self._add(str(), self.name)
		#self._add(str(), WStringToUTF8(self.description))
		self._add(str(), self.description)

		self._add(ctypes.c_uint16, len(self.parameters))
		for item in self.parameters:
			self._add(ctypes.c_int16, item['size'])
			#self._add(str(), WStringToUTF8(item['name']))
			self._add(str(), item['name'])

	def _deserializeSpecific(self):
		#self.name = UTF8ToWString(self._get(str()))
		self.name = self._get(str())
		#self.description = UTF8ToWString(self._get(str()))
		self.description = self._get(str())

		size = self._get(ctypes.c_uint16)
		self.parameters = []
		for j in range(size):
			item = { 'size': self._get(ctypes.c_int16), 
			         #'name': UTF8ToWString(self._get(str())), }
			         'name': self._get(str()), }
			self.parameters.append(item)


#//! Static class that fills a table of known messages types in its constructor
class MessageTypesInitializer():
	_messagesTypes = {}		#//!< table of known messages types
	
	#//! Constructor, register all known messages types
	def __init__(self):
		self.registerMessageType(Description, consts.ASEBA_MESSAGE_DESCRIPTION)
		self.registerMessageType(NamedVariableDescription, consts.ASEBA_MESSAGE_NAMED_VARIABLE_DESCRIPTION)
		self.registerMessageType(LocalEventDescription, consts.ASEBA_MESSAGE_LOCAL_EVENT_DESCRIPTION)
		self.registerMessageType(NativeFunctionDescription, consts.ASEBA_MESSAGE_NATIVE_FUNCTION_DESCRIPTION)
		#registerMessageType<Disconnected>(ASEBA_MESSAGE_DISCONNECTED);
		#registerMessageType<Variables>(ASEBA_MESSAGE_VARIABLES);
		#registerMessageType<ArrayAccessOutOfBounds>(ASEBA_MESSAGE_ARRAY_ACCESS_OUT_OF_BOUNDS);
		#registerMessageType<DivisionByZero>(ASEBA_MESSAGE_DIVISION_BY_ZERO);
		#registerMessageType<EventExecutionKilled>(ASEBA_MESSAGE_EVENT_EXECUTION_KILLED);
		#registerMessageType<NodeSpecificError>(ASEBA_MESSAGE_NODE_SPECIFIC_ERROR);
		#registerMessageType<ExecutionStateChanged>(ASEBA_MESSAGE_EXECUTION_STATE_CHANGED);
		#registerMessageType<BreakpointSetResult>(ASEBA_MESSAGE_BREAKPOINT_SET_RESULT);
		
		#registerMessageType<GetDescription>(ASEBA_MESSAGE_GET_DESCRIPTION);
		
		#//registerMessageType<AttachDebugger>(ASEBA_MESSAGE_ATTACH_DEBUGGER);
		#//registerMessageType<DetachDebugger>(ASEBA_MESSAGE_DETACH_DEBUGGER);
		#registerMessageType<SetBytecode>(ASEBA_MESSAGE_SET_BYTECODE);
		#registerMessageType<Reset>(ASEBA_MESSAGE_RESET);
		#registerMessageType<Run>(ASEBA_MESSAGE_RUN);
		#registerMessageType<Pause>(ASEBA_MESSAGE_PAUSE);
		#registerMessageType<Step>(ASEBA_MESSAGE_STEP);
		#registerMessageType<Stop>(ASEBA_MESSAGE_STOP);
		#registerMessageType<GetExecutionState>(ASEBA_MESSAGE_GET_EXECUTION_STATE);
		#registerMessageType<BreakpointSet>(ASEBA_MESSAGE_BREAKPOINT_SET);
		#registerMessageType<BreakpointClear>(ASEBA_MESSAGE_BREAKPOINT_CLEAR);
		#registerMessageType<BreakpointClearAll>(ASEBA_MESSAGE_BREAKPOINT_CLEAR_ALL);
		#registerMessageType<GetVariables>(ASEBA_MESSAGE_GET_VARIABLES);
		#registerMessageType<SetVariables>(ASEBA_MESSAGE_SET_VARIABLES);
		#registerMessageType<WriteBytecode>(ASEBA_MESSAGE_WRITE_BYTECODE);
		#registerMessageType<Reboot>(ASEBA_MESSAGE_REBOOT);
		#registerMessageType<Sleep>(ASEBA_MESSAGE_SUSPEND_TO_RAM);
		
	#//! Register a message type by storing a pointer to its constructor
	def registerMessageType(self, Sub, t):
		self._messagesTypes[t] = Sub
		
	#//! Create an instance of a registered message type
	def createMessage(self, t):
		return self._messagesTypes.get(t, UserMessage)()
		
		#//! Print the list of registered messages types to stream
		#void dumpKnownMessagesTypes(wostream &stream) const
		#{
			#stream << hex << showbase;
			#for (map<uint16, CreatorFunc>::const_iterator it = messagesTypes.begin(); it != messagesTypes.end(); ++it)
				#stream << "\t" << setw(4) << it->first << "\n";
			#stream << dec << noshowbase;
		#}
