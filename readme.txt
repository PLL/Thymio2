
ASEBA (ThymioII) Serial USB Interface for Python and LabView

In order to be able to port this to other languages and keep it simple
here a minimal approach.

Please confer also:
  * https://aseba.wikidot.com
  * https://aseba.wikidot.com/de:thymioprogram
  * https://aseba.wikidot.com/forum/t-655386
  * https://aseba.wikidot.com/forum/t-644927
  * https://github.com/aseba-community/aseba/tree/master/common
  * http://www.ni.com/white-paper/3573/en/

Besides the fact that the source is well implemented object-oriented
C/C++ code, in order to be able to port it to LabView also a procedural
approach was needed - for 2 reasons:
  * understand what happens on the USB/serial line (bit banging)
    this is may be also be educative or illustrative for other developers
    (it was for me ;)
  * it's a pitty but LabView does not *really* support object-oriented
    programming (e.g. lack of templates, pure virtual functions and multiple
    inheritance)


"python-serial" therefore contains two examples scripts, each representing
one approach. Both of them need the additional python module "pyserial"
which is known to work under and available for linux/unix, macos and win.
Therefore this approach is "ready-to-use" besides the fact that not all
functionality has been implemented yet.

== aseba-thymio2-lowlevel.py (python) ==

Represents the imperative / procedural programming approach:

  $ cd .../Thymio2/python-serial/
  $ cp aseba/examples/clients/python-serial/aseba-thymio2-lowlevel.py aseba-thymio2-lowlevel.py
  $ python aseba-thymio2-lowlevel.py

RECOMMENDED: For introduction and users that want to understand what
happens on the serial line, e.g. in order to further develop the code
below. Remember Wireshark and USBPcap are your friends.

== aseba-thymio2.py (python) ==

Represents the proper objec-oriented design according to aseba source:

  $ cd .../Thymio2/python-serial/
  $ cp aseba/examples/clients/python-serial/aseba-thymio2.py aseba-thymio2.py
  $ python aseba-thymio2.py 

RECOMMENDED: For users that want to talk to ThymioII by serial port
from Python on any OS.


"LV-serial" also contains two examples scripts, each representing one
approach. LabView is needed in order to use them.

== Aseba ThymioII USB Serial Interface lowlevel.llb (LabView) ==

Again the imperative / procedural programming approach. This is what is
needed for our project (apprentice education), the party is most advanced
and maintained.

RECOMMENDED: For users that want to talk to ThymioII from LabView.

( == Aseba ThymioII USB Serial Interface.llb (LabView) ==                  )
(                                                                          )
( Now this is supposed to be the proper "objec-oriented" design as similar )
( to aseba source as possible. LabView does support OO, but lacks of some  )
( important concepts (as templates and more - mentioned before), therefore )
( this will always deviate from aseba sources.                             )
(                                                                          )
( NOT RECOMMENDED: ...since not working at the moment.                     )

