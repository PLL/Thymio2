port = 'COM4'        # COM4 (win)
#port = '/dev/ttyS1'  # ttyS1 (unix/linux)

import serial

import aseba
import aseba.common
#import aseba.common.consts
import aseba.common.msg.msg
#import aseba.common.utils.utils

try:
    # open serial port with thymio2 connected
    ser = serial.Serial(port,
                        baudrate=115200,
                        bytesize=8,
                        parity='N',
                        stopbits=1,
                        xonxoff=0, rtscts=0)
    # check which port was really used
    print "port:", ser.portstr

    #import StringIO
    #ser = StringIO.StringIO()

    # serialize and send 'GetDescription' message
    out_msg = aseba.common.msg.msg.GetDescription()
    out_msg.serialize(ser)
    #print 'sent: %r' % ser.getvalue()

    #ser = open('test-buffer', 'rb')

    # receive and deserialize answer
    check_hardware = False
    in_msgs = aseba.common.msg.msg.Message()	# take generic message
    for i in range(94):
        msg = in_msgs.receive(ser)
        print msg
        if   isinstance(msg, aseba.common.msg.msg.Description):
            print 'name:', msg.name
            print 'protocolVersion:', msg.protocolVersion

            print 'bytecodeSize:', msg.bytecodeSize
            print 'stackSize:', msg.stackSize
            print 'variablesSize:', msg.variablesSize

            print 'namedVariables_size:', msg.namedVariables_size
            print 'localEvents_size:', msg.localEvents_size
            print 'nativeFunctions_size:', msg.nativeFunctions_size

            check_hardware = (msg.name == 'thymio-II')
        elif isinstance(msg, aseba.common.msg.msg.NamedVariableDescription):
            print 'size:', msg.size
            print 'name:', msg.name
        elif isinstance(msg, aseba.common.msg.msg.LocalEventDescription):
            print 'name:', msg.name
            print 'description:', msg.description
        elif isinstance(msg, aseba.common.msg.msg.NativeFunctionDescription):
            print 'name:', msg.name
            print 'description:', msg.description

            print 'size:', len(msg.parameters)
            for item in msg.parameters:
                print '', 'item:', item
        else:
            raise NotImplementedError

    if check_hardware:
        print "\nRESULT: ThymioII found."
except serial.serialutil.SerialException:
    print "ERROR: port could not be opened!"
