port = 'COM4'        # COM4 (win)
#port = '/dev/ttyS1'  # ttyS1 (unix/linux)
ASEBA_MESSAGE_DESCRIPTION = 0x9000                  # Description
ASEBA_MESSAGE_NAMED_VARIABLE_DESCRIPTION = 0x9001   # NamedVariableDescription
ASEBA_MESSAGE_LOCAL_EVENT_DESCRIPTION = 0x9002      # LocalEventDescription
ASEBA_MESSAGE_NATIVE_FUNCTION_DESCRIPTION = 0x9003  # NativeFunctionDescription
ASEBA_MESSAGE_DISCONNECTED = 0x9004                 #
ASEBA_MESSAGE_VARIABLES = 0x9005                    #
ASEBA_MESSAGE_ARRAY_ACCESS_OUT_OF_BOUNDS = 0x9006   #
ASEBA_MESSAGE_DIVISION_BY_ZERO = 0x9007             #
ASEBA_MESSAGE_EVENT_EXECUTION_KILLED = 0x9008       #
ASEBA_MESSAGE_NODE_SPECIFIC_ERROR = 0x9009          #
ASEBA_MESSAGE_EXECUTION_STATE_CHANGED = 0x900A      #
ASEBA_MESSAGE_BREAKPOINT_SET_RESULT = 0x900B        #
ASEBA_MESSAGE_GET_DESCRIPTION = 0xA000              # GetDescription

MESSAGE = "\x04\x00"    # ?????

import serial
import struct

try:
    # open serial port with thymio2 connected
    ser = serial.Serial(port,
                        baudrate=115200,
                        bytesize=8,
                        parity='N',
                        stopbits=1,
                        xonxoff=0, rtscts=0)
    # check which port was really used
    print "port:", ser.portstr

    # serialize and send MESSAGE
    #print '%r' % struct.pack('<H', len(MESSAGE))
    #print '%r' % struct.pack('<H', 0)
    #print '%r' % struct.pack('<H', ASEBA_MESSAGE_GET_DESCRIPTION)
    #print '%r' % MESSAGE
    # header
    ser.write(struct.pack('<H', len(MESSAGE)))                   # length of message (word)
    ser.write(struct.pack('<H', 0))                              # source id of message (word)
    ser.write(struct.pack('<H', ASEBA_MESSAGE_GET_DESCRIPTION))  # type of message (word)
    # payload
    ser.write(MESSAGE)                                           # the message ...

    #ser = open('test-buffer', 'rb')

    # receive and deserialize answer
    check_hardware = False
    size = 0
    while size < 4549:
        # header
        length = struct.unpack('<H', ser.read(2))[0]
        size += 2
        sid  = struct.unpack('<H', ser.read(2))[0]
        size += 2
        styp = struct.unpack('<H', ser.read(2))[0]
        size += 2
        # payload ...
        msg  = ser.read(length)
        if   styp == ASEBA_MESSAGE_DESCRIPTION:                  # Description
            readPos = 0
            l = ord(msg[readPos])           # get<string>; Pascal-style string
            s = msg[readPos+1:readPos+1+l]  #
            print 'name:', s, len(s)        #
            readPos += 1+l                  #
            #print ''.join([struct.pack('<H', ord(a)) for a in s.decode('UTF8')]) # UTF8ToWString
            val = msg[readPos:readPos+2]                                     # get<uint16>
            print 'protocolVersion:', struct.unpack('<H', val)[0], len(val)  #
            readPos += 2                                                     #

            check_hardware = (s == "thymio-II") # check for thymio signature

            val = msg[readPos:readPos+2]                                     # get<uint16>
            print 'bytecodeSize:', struct.unpack('<H', val)[0], len(val)     #
            readPos += 2                                                     #
            val = msg[readPos:readPos+2]                                     # get<uint16>
            print 'stackSize:', struct.unpack('<H', val)[0], len(val)        #
            readPos += 2                                                     #
            val = msg[readPos:readPos+2]                                     # get<uint16>
            print 'variablesSize:', struct.unpack('<H', val)[0], len(val)    #
            readPos += 2                                                     #

            val = msg[readPos:readPos+2]                                     # get<uint16>
            print 'namedVariables:', struct.unpack('<H', val)[0], len(val)   #
            readPos += 2                                                     #
            #// named variables are received separately

            val = msg[readPos:readPos+2]                                     # get<uint16>
            print 'localEvents:', struct.unpack('<H', val)[0], len(val)      #
            readPos += 2                                                     #
            #// local events are received separately

            val = msg[readPos:readPos+2]                                     # get<uint16>
            print 'nativeFunctions:', struct.unpack('<H', val)[0], len(val)  #
            readPos += 2                                                     #
            #// native functions are received separately

            assert(readPos == length)
        elif styp == ASEBA_MESSAGE_NAMED_VARIABLE_DESCRIPTION:   # NamedVariableDescription
            readPos = 0
            val = msg[readPos:readPos+2]                          # get<uint16>
            print 'size:', struct.unpack('<H', val)[0], len(val)  #
            readPos += 2                                          #
            l = ord(msg[readPos])           # get<string>; Pascal-style string
            s = msg[readPos+1:readPos+1+l]  #
            print 'name:', s, len(s)        #
            readPos += 1+l                  #

            assert(readPos == length)
        elif styp == ASEBA_MESSAGE_LOCAL_EVENT_DESCRIPTION:      # LocalEventDescription
            readPos = 0
            l = ord(msg[readPos])           # get<string>; Pascal-style string
            s = msg[readPos+1:readPos+1+l]  #
            print 'name:', s, len(s)        #
            readPos += 1+l                  #
            l = ord(msg[readPos])           # get<string>; Pascal-style string
            s = msg[readPos+1:readPos+1+l]  #
            print 'description:', s, len(s) #
            readPos += 1+l                  #

            assert(readPos == length)
        elif styp == ASEBA_MESSAGE_NATIVE_FUNCTION_DESCRIPTION:  # NativeFunctionDescription
            readPos = 0
            l = ord(msg[readPos])           # get<string>; Pascal-style string
            s = msg[readPos+1:readPos+1+l]  #
            print 'name:', s, len(s)        #
            readPos += 1+l                  #
            l = ord(msg[readPos])           # get<string>; Pascal-style string
            s = msg[readPos+1:readPos+1+l]  #
            print 'description:', s, len(s) #
            readPos += 1+l                  #

            val = msg[readPos:readPos+2]                                # get<uint16>
            print 'parameters:', struct.unpack('<H', val)[0], len(val)  #
            readPos += 2                                                #
            for i in range(struct.unpack('<H', val)[0]):
                val = msg[readPos:readPos+2]                                # get<sint16>
                print '', 'size:', struct.unpack('<h', val)[0], len(val)    #
                readPos += 2                                                #
                l = ord(msg[readPos])           # get<string>; Pascal-style string
                s = msg[readPos+1:readPos+1+l]  #
                print '', 'name:', s, len(s)    #
                readPos += 1+l                  #

            assert(readPos == length)
        else:
            raise IOError("Message type not implemted yet!")
        size += len(msg)

    if check_hardware:
        print "\nRESULT: ThymioII found."
except serial.serialutil.SerialException:
    print "ERROR: port could not be opened!"
